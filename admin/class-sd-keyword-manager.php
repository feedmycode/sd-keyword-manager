<?php
/**
 * Southern Destinations Keyword Manager
 *
 * @package   SD_Keyword_Manager
 * @author    LightSpeed <info@lsdev.biz>
 * @license   GPL-2.0+
 * @link      http://bootstraptourism.com
 * @copyright 2014 LightSpeed
 */

/**
 * @package SD_Keyword_Manager
 * @author  Your Name <email@example.com>
 */
class SD_Keyword_Manager {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @since   1.0.0
	 *
	 * @var     string
	 */
	const VERSION = '1.0.0';

	/**
	 * The variable name is used as the text domain when internationalizing strings
	 * of text. Its value should match the Text Domain file header in the main
	 * plugin file.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_slug = 'bst-keyword-manager';

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Slug of the plugin screen.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_screen_hook_suffix = null;

	/**
	 * Initialize the plugin by setting localization and loading public scripts
	 * and styles.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		// Load admin style sheet and JavaScript.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

		add_action( 'admin_menu', array( $this, 'register_settings_page' ) );
		add_filter( 'set-screen-option', array( $this, 'keyword_table_set_option' ), 10, 3);
	}

	/**
	 * Return the plugin slug.
	 *
	 * @since    1.0.0
	 *
	 * @return    Plugin slug variable.
	 */
	public function get_plugin_slug() {
		return $this->plugin_slug;
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Fired when the plugin is activated.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $searchwp;
		
		$swp_options = searchwp_get_option( 'settings' );

		$swp_options['engines']['default']['tour']['weights']['tax']['tour_keyword'] = 50;
		$swp_options['engines']['default']['accommodation']['weights']['tax']['accommodation_keyword'] = 50;

		searchwp_update_option( 'settings', $swp_options );
	}

	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		global $searchwp;
		
		$swp_options = searchwp_get_option( 'settings' );

		unset($swp_options['engines']['default']['tour']['weights']['tax']['tour_keyword']);
		unset($swp_options['engines']['default']['accommodation']['weights']['tax']['accommodation_keyword']);

		searchwp_update_option( 'settings', $swp_options );
	}

	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 * @since     1.0.0
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_styles() {

		if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
			return;
		}

		$screen = get_current_screen();
		if ( $this->plugin_screen_hook_suffix == $screen->id ) {
			 wp_enqueue_style( $this->plugin_slug .'-admin-styles', plugins_url( 'assets/css/admin.css', __FILE__ ), array(), SD_Keyword_Manager::VERSION );
		}

	} 

	/** 
	 * @since     1.0.0
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_scripts() {

		if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
			return;
		}

		$screen = get_current_screen(); 

		if ( $this->plugin_screen_hook_suffix == $screen->id ) {
			wp_enqueue_script( $this->plugin_slug . '-admin-script', plugins_url( 'assets/js/admin.js', __FILE__ ), array( 'jquery' ), SD_Keyword_Manager::VERSION );
			wp_enqueue_script( 'select2', plugins_url( 'assets/js/vendor/select2.min.js', __FILE__ ), array( 'jquery' ), '3.4.8' );
		}
	}


	public function register_settings_page() {
		$this->plugin_screen_hook_suffix = add_submenu_page( 'options-general.php', 'Bootstrap Tourism Keyword Manager', 'Bootstrap Tourism Keyword Manager', 'manage_options', 'bst-keyword-manager', array( $this, 'settings_page_callback' ) ); 
		add_action( "load-$this->plugin_screen_hook_suffix", array( $this, 'add_screen_options' ) );
	}

	public function add_screen_options() {
		$option = 'per_page';
		$args = array(
		     'label' => 'Items',
		     'default' => 25,
		     'option' => 'keyword_items_per_page'
		     );
		add_screen_option( $option, $args );
	}

	function keyword_table_set_option($status, $option, $value) {
		return $value;
	}

	public function settings_page_callback() {

	    if ( ! empty( $_POST['save'] ) ) {
	    	$save_terms = $_POST['save_keywords'];
	    	$post_ids = $_POST['empty_keyword'];
	    	$post_ids = array_map( 'intval', $post_ids );
			$post_ids = array_unique( $post_ids );

			foreach ( $post_ids as $post_id ) {
				$post = get_post( $post_id );

				if ( array_key_exists($post_id, $save_terms) ) {
					$term_ids = $save_terms[$post_id];
					$term_ids = array_map( 'intval', $term_ids );
					$term_ids = array_unique( $term_ids );					
					$term = wp_get_object_terms( $post_id, 'product_cat' );								

					wp_set_object_terms( $post_id, $term_ids, $term[0]->slug . '_keyword', true );
				} else {
					wp_set_object_terms( $post_id, null, $term[0]->slug . '_keyword' );
		    	}
			}
		}

	    //Create an instance of our package class...
	    $keywordListTable = new Keyword_Manager_List_Table();
	    //Fetch, prepare, sort, and filter our data...
	    if( isset($_POST['s']) ){
                $keywordListTable->prepare_items($_POST['s']);
        } else {
                $keywordListTable->prepare_items();
        }
	    
	    ?>
	    <div class="wrap">
	        
	        <div id="icon-users" class="icon32"><br/></div>
	        <h2>Bootstrap Tourism Keyword Manager</h2>
	        
	        <form method="get">
			    <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
			    <?php $keywordListTable->search_box('search', 'search_id'); ?>
			    
			    <p autocomplete="off" class="search-box">
				    <?php if(isset($_REQUEST['keyword_tour'])) { $tour_keyword = $_REQUEST['keyword_tour']; }else{ $tour_keyword = 0; }?>
				    				    
					<select autocomplete="off" name="keyword_tour">
					
						<option value="">Tour Keywords</option>
						<?php 
						$tour_keywords = get_terms( 'tour_keyword', array( 'hide_empty' => 0 ) );
						foreach ( $tour_keywords as $tk) {
							if ( $tour_keyword == $tk->term_id ) {
								$selected = ' selected="selected"';
							} else {
								$selected = '';
							}
							echo '<option value=' . $tk->term_id . $selected . '>' . $tk->name . '</option>';
						}
						?>	
					</select>
				</p>
				
				<p class="search-box">
				    <?php if(isset($_REQUEST['keyword_accommodation'])) { $accommodation_keyword = $_REQUEST['keyword_accommodation']; }else{ $accommodation_keyword = 0; }?>
					<select autocomplete="off" name="keyword_accommodation">
						<option value="">Accommodation Keywords</option>
						<?php 
						$accommodation_keywords = get_terms( 'accommodation_keyword', array( 'hide_empty' => 0 ) );
						foreach ( $accommodation_keywords as $ak) {
							if ( $accommodation_keyword == $ak->term_id ) {
								$selected = ' selected="selected"';
							} else {
								$selected = '';
							}
							echo '<option value=' . $ak->term_id . $selected . '>' . $ak->name . '</option>';
						}
						?>
					</select>
				</p>
			    
			    <p class="search-box">
				    <?php if(isset($_REQUEST['type'])) { $type = $_REQUEST['type']; }else{ $type = 0; }?>
					<select name="type">
						<option <?php selected('0', $type); ?> value="">Both</option>
						<option <?php selected('accommodation', $type); ?> value="accommodation">Accommodation</option>
						<option <?php selected('tour', $type); ?> value="tour">Tour</option>
					</select>
				</p>			    
			    
			</form>
	        <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
	        <form id="keyword-filter" method="post">
		        <div class="alignleft" style="padding: 3px 0 0 8px;">
					<input type="submit" name="save" value="<?php _e( 'Save keywords', 'bst-keyword-manager' ); ?>" class="button button-primary" />
				</div>
	            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
	            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
	            <!-- Now we can render the completed list table -->
	            <?php $keywordListTable->display(); ?>
	            
	        </form>
	        
	    </div>
	    <?php
	}

	public function searchwp_keyword_configuration() {
		global $searchwp;

		$swp_options = searchwp_get_option( 'settings' );

		$swp_options['engines']['default']['tour']['tax']['tour_keyword'] = 100;
		$swp_options['engines']['default']['accommodation']['tax']['accommodation_keyword'] = 100;

		searchwp_update_option( 'settings', $swp_options );
	}
}