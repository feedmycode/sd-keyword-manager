<?php
/**
 * Keyword_Manager_List_Table.
 *
 * @package   SD_Keyword_Manager
 * @author    LightSpeed <info@lsdev.biz>
 * @license   GPL-2.0+
 * @link      http://bootstraptourism.com
 * @copyright 2014 LightSpeed
 */

/**
 * @package Keyword_Manager_List_Table
 */
class Keyword_Manager_List_Table extends WP_List_Table {

	/**
	 * Constructor
	 */
	public function __construct() {

		parent::__construct( array(
			'singular' => 'keyword',     //singular name of the listed records
			'plural'   => 'keywords',    //plural name of the listed records
			'ajax'     => true //false        	 //does this table support ajax?
		) );
	}

	/**
	 * Output column data
	 * @param  object $post
	 * @param  string $column_name
	 * @return string
	 */
	public function column_default( $post, $column_name ) {
		switch ( $column_name ) {
            case 'type':
            	$term = wp_get_object_terms( $post->ID, 'product_cat' );
                return $term[0]->name;
            default:
                return print_r($post,true); //Show the whole array for troubleshooting purposes
        }
	}

	/**
	 * Handle the output of the title column
	 * @param  object $post
	 * @return string
	 */
	public function column_title ( $post ) {
        
        //Return the title contents
        return sprintf('%1$s <span style="color:silver">(id:%2$s)</span>',
            /*$1%s*/ $post->post_title,
            /*$2%s*/ $post->ID
        );
    }

	public function column_search_keywords( $item ) {
		// global $post;

		$term = wp_get_object_terms( $item->ID, 'product_cat' );		
		$term_slug = $term[0]->slug;
		$item_terms = wp_get_post_terms($item->ID, $term_slug . '_keyword', array( 'fields' => 'ids') );
		$all_terms = get_terms( $term_slug . '_keyword', array( 'hide_empty' => 0, 'fields' => 'ids' ) );

		$item_terms = array_map( 'intval', $item_terms );

		$html = '<select id="keywords-' . $item->ID . '" class="multiple-select" name="save_keywords[' . $item->ID . '][]" multiple="multiple" style="width:100%;">';

		foreach ( $all_terms as $term ) {
			if ( in_array( intval($term), $item_terms ) ) {
				$selected = ' selected="selected"';
			} else {
				$selected = '';
			}
			$term_object = get_term( $term, $term_slug . '_keyword' );
			$html .= '<option value=' . $term . $selected . '>' . $term_object->name . '</option>';
		}

		$html .= '</select>';  
		$html .= '<input type="hidden" name="empty_keyword[]" value="' . $item->ID . '" />';
		return $html;   
	}

	/**
	 * Get columns
	 * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
	 */
	public function get_columns(){
		$columns = array(
			'title'       => __( 'Name', 'bst-keyword-manager' ),
			'type'        => __( 'Type', 'bst-keyword-manager' ),
			'search_keywords'    => __( 'Keywords', 'bst-keyword-manager' ),
		);

		return $columns;
	}

	/**
	 * If you want one or more columns to be sortable (ASC/DESC toggle),
	 * you will need to register it here. This should return an array where the
	 * key is the column that needs to be sortable, and the value is db column to
	 * sort by. Often, the key and value will be the same, but this is not always
	 * the case (as the value is a column name from the database, not the list table).
	 *
	 * This method merely defines which columns should be sortable and makes them
	 * clickable - it does not handle the actual sorting. You still need to detect
	 * the ORDERBY and ORDER querystring variables within prepare_items() and sort
	 * your data accordingly (usually by modifying your query).
	 *
	 * @return array An associative array containing all the columns that should be sortable: 'slugs'=>array('data_values',bool)
	 */
	public function get_sortable_columns() {
		$sortable_columns = array(
			'title'     => array( 'title', true ),
		);
		return $sortable_columns;
	}


	/**
	 * Get items to display
	 */
	public function prepare_items($search = false) {

		$items = get_posts( array( 'post_type' => array( 'product' ), 'posts_per_page' => -1 ) );
		$total_items = count($items);
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array($columns, $hidden, $sortable);

		$per_page = $this->get_items_per_page('keyword_items_per_page', 25);

		$page_args = array();
		
		if ( isset($_REQUEST['paged']) ) {
			$offset = $per_page * ( intval($_REQUEST['paged']) - 1);
		} else {
			$offset = 0;
		}
		$page_args['posts_per_page'] = $per_page;
				

		if ( isset($_REQUEST['order']) ) {
			$order = $_REQUEST['order'];
		} else {
			$order = 'DESC';
		}
		$page_args['order'] = $order;
				

		if ( isset($_REQUEST['orderby']) ) {
			$orderby = $_REQUEST['orderby'];
		} else {
			$orderby = 'post_date';
		}
		$page_args['orderby'] = $orderby;
		
		
		if ( isset($_REQUEST['keyword_tour']) && '' != $_REQUEST['keyword_tour']) {
			$keyword_tour = $_REQUEST['keyword_tour'];
			$page_args['tax_query'][] = array(
											'taxonomy' => 'tour_keyword',
											'field'		=> 'term_id',
											'terms'		=> array($keyword_tour)
										);
		} 

		if ( isset($_REQUEST['keyword_accommodation']) && '' != $_REQUEST['keyword_accommodation']) {
			$keyword_accommodation = $_REQUEST['keyword_accommodation'];
			$page_args['tax_query'][] = array(
					'taxonomy' => 'accommodation_keyword',
					'field'		=> 'term_id',
					'terms'		=> array($keyword_accommodation)
			);
		}
				

		if ( isset($_REQUEST['s']) && '' != $_REQUEST['s'] ) {
			$page_args['s'] = $_REQUEST['s'];
		}
		
		
		if ( isset($_REQUEST['type']) && '' != $_REQUEST['type']) {
			$post_type = array( 'product' );
		} else {
			$post_type = array( 'product' );
		}
		$page_args['post_type'] = $post_type;
		
		$this->items = get_posts( $page_args );
		
		$this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );

	}
}