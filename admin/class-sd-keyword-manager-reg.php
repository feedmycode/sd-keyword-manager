<?php
/**
 * Bootstrap Tourism Search.
 *
 * @package   SD_Keyword_Manager_Reg
 * @author    LightSpeed <info@lsdev.biz>
 * @license   GPL-2.0+
 * @link      http://bootstraptourism.com
 * @copyright 2014 LightSpeed
 */

/**
 * @package SD_Keyword_Manager_Reg
 * @author  Your Name <email@example.com>
 */
class SD_Keyword_Manager_Reg {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Initialize the plugin by setting localization and loading public scripts
	 * and styles.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		// Register and attach keyword taxonomies
		add_action( 'init', array( $this, 'register_keyword_taxonomies' ) );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function register_keyword_taxonomies() {
		// Register tour keyword taxonomy
		$labels = array(
			'name'                       => _x( 'Tour Keywords', 'taxonomy general name' ),
			'singular_name'              => _x( 'Tour Keyword', 'taxonomy singular name' ),
			'search_items'               => __( 'Search Tour Keywords' ),
			'popular_items'              => __( 'Popular Tour Keywords' ),
			'all_items'                  => __( 'All Tour Keywords' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit Tour Keyword' ),
			'update_item'                => __( 'Update Tour Keyword' ),
			'add_new_item'               => __( 'Add New Tour Keyword' ),
			'new_item_name'              => __( 'New Tour Keyword Name' ),
			'separate_items_with_commas' => __( 'Separate tour keywords with commas' ),
			'add_or_remove_items'        => __( 'Add or remove tour keywords' ),
			'choose_from_most_used'      => __( 'Choose from the most used tour keywords' ),
			'not_found'                  => __( 'No tour keywords found.' ),
			'menu_name'                  => __( 'Tour Keywords' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'tour-keyword' ),
		);

		register_taxonomy( 'tour_keyword', array( 'tour' ), $args );

		
		// Register accommodation keyword taxonomy
		$labels = array(
			'name'                       => _x( 'Accommodation Keywords', 'taxonomy general name' ),
			'singular_name'              => _x( 'Accommodation Keyword', 'taxonomy singular name' ),
			'search_items'               => __( 'Search Accommodation Keywords' ),
			'popular_items'              => __( 'Popular Accommodation Keywords' ),
			'all_items'                  => __( 'All Accommodation Keywords' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit Accommodation Keyword' ),
			'update_item'                => __( 'Update Accommodation Keyword' ),
			'add_new_item'               => __( 'Add New Accommodation Keyword' ),
			'new_item_name'              => __( 'New Accommodation Keyword Name' ),
			'separate_items_with_commas' => __( 'Separate accommodation keywords with commas' ),
			'add_or_remove_items'        => __( 'Add or remove accommodation keywords' ),
			'choose_from_most_used'      => __( 'Choose from the most used accommodation keywords' ),
			'not_found'                  => __( 'No accommodation keywords found.' ),
			'menu_name'                  => __( 'Accommodation Keywords' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'accommodation-keyword' ),
		);

		register_taxonomy( 'accommodation_keyword', array( 'accommodation' ), $args );
	}
}