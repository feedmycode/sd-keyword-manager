<?php
/**
 * Southern Destinations Keyword Manager
 *
 *
 * @package   SD_Keyword_Manager
 * @author    LightSpeed <info@lsdev.biz>
 * @license   GPL-2.0+
 * @link      http://bootstraptourism.com
 * @copyright 2014 LightSpeed
 *
 * @wordpress-plugin
 * Plugin Name:       Southern Destinations Keyword Manager
 * Plugin URI:        http://bootstraptourism.com
 * Description:       Search Keyword Manager add-on for Bootstrap Tourism
 * Version:           1.0.0
 * Author:            Bootstrap Tourism
 * Author URI:        http://bootstraptourism.com
 * Text Domain:       sd-search
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * WordPress-Plugin-Boilerplate: v2.6.1
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'admin/class-sd-keyword-manager.php' );

// require_once( plugin_dir_path( __FILE__ ) . 'admin/class-sd-keyword-manager-reg.php' );
// add_action( 'plugins_loaded', array( 'SD_Keyword_Manager_Reg', 'get_instance' ) );

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
if ( ! class_exists( 'Keyword_Manager_List_Table' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-sd-keyword-manager-list-table.php' );
}
/*
 * Register hooks that are fired when the plugin is activated or deactivated.
 * When the plugin is deleted, the uninstall.php file is loaded.
 */
register_activation_hook( __FILE__, array( 'SD_Keyword_Manager', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'SD_Keyword_Manager', 'deactivate' ) );

add_action( 'plugins_loaded', array( 'SD_Keyword_Manager', 'get_instance' ) );

